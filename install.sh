#!/bin/bash

set -e
set -u
# git clone https://git.oauth3.org/OAuth3/issuer.html.git /srv/walnut/packages/pages/issuer@oauth3.org
# git clone https://git.oauth3.org/OAuth3/azp.html.git /srv/walnut/packages/pages/azp@oauth3.org

mkdir -p assets
if ! [ -d ./assets/oauth3.org ]; then
  git clone https://git.oauth3.org/OAuth3/oauth3.js.git ./assets/oauth3.org
fi
pushd ./assets/oauth3.org
  git checkout v1.2
  git pull
popd

bower install rsvp
