(function () {
  'use strict';

  ////////////////////////
  //                    //
  //  3rd Party Logins  //
  //                    //
  ////////////////////////
  util.facebookLogin = function (ev) {
    ev.preventDefault();
    ev.stopPropagation();

    var ppid = '';

    var authObj = window.OAUTH3.core.implicitGrant(
      { authorization_dialog: { url:'https:////www.facebook.com/v2.8/dialog/oauth' }
      }
    , { redirectUri: 'https://' + CONFIG.host + '/.well-known/oauth3/callback.html'
      , appId: '590912911107527'
      }
    );

    var signinW = window.open(
      authObj.url
    , "third-party-provider"
    );

    var callbackName = '--oauth3-callback-' + authObj.state;

    console.log('authobj: ', authObj);
    console.log('callback: ',callbackName);

    window[callbackName] = function (obj) {
      console.log('callback obj: ', obj);
      $.ajax({
        url: "https://graph.facebook.com/me"
      , headers: {authorization: "Bearer " + obj.access_token}
      }).then( function (ajaxObj) {
        console.log("Ajax obj: ", ajaxObj);
        ppid = ajaxObj.id;
      });
    };
  };
  util.twitterLogin = function (ev) {
    ev.preventDefault();
    ev.stopPropagation();

    var authObj = window.OAUTH3.core.implicitGrant(
      { authorization_dialog: { url:'https://api.twitter.com/oauth/authorize' }
      }
    , { redirectUri: 'https://' + CONFIG.host + '/.well-known/oauth3/callback.html'
      , appId: 'HJuxVttZUX4kPmbafvzmcuU1O'
      }
    );

    var signinW = window.open(
      authObj.url
    , "third-party-provider"
    );

    var callbackName = '--oauth3-callback-' + authObj.state;

    console.log('authobj: ', authObj);
    console.log('callback: ',callbackName);

    window[callbackName] = function (obj) {
      console.log('callback obj: ', obj);
    };
  };
  util.googleLogin = function (ev) {
    ev.preventDefault();
    ev.stopPropagation();

    var authObj = window.OAUTH3.core.implicitGrant(
      { authorization_dialog: { url:'https://accounts.google.com/o/oauth2/v2/auth' }
      }
    , { redirectUri: 'https://' + CONFIG.host + '/.well-known/oauth3/callback.html'
      , appId: '458817232132-sdk9eioi22k36jqpj0mq3i89h6tsohut.apps.googleusercontent.com'
      , scope: 'profile'
      }
    );

    var signinW = window.open(
      authObj.url
    , "third-party-provider"
    );

    var callbackName = '--oauth3-callback-' + authObj.state;

    console.log('authobj: ', authObj);
    console.log('callback: ',callbackName);

    window[callbackName] = function (obj) {
      console.log('callback obj: ', obj);
      $.ajax({
        url: "https://www.googleapis.com/oauth2/v3/userinfo"
      , headers: {authorization: "Bearer " + obj.access_token}
      }).then( function (ajaxObj) {
        console.log("Ajax obj: ", ajaxObj);
      });
    };
  };
  util.githubLogin = function (ev) {
    ev.preventDefault();
    ev.stopPropagation();

    var ppid = '';

    var authObj = window.OAUTH3.core.implicitGrant(
      { authorization_dialog: { url:'https://github.com/login/oauth/authorize' }
      }
    , { redirect_uri: 'https://' + CONFIG.host + '/.well-known/oauth3/callback.html'
      , client_id: 'df4d46a358c1f3519c60'
      }
    );

    var signinW = window.open(
      authObj.url
    , "third-party-provider"
    );

    var callbackName = '--oauth3-callback-' + authObj.state;

    console.log('authobj: ', authObj);
    console.log('callback: ',callbackName);

    window[callbackName] = function (obj) {
      console.log('callback obj: ', obj);
      $.ajax({
        url: "https://api.github.com/user"
      , headers: {authorization: "Bearer " + obj.access_token}
      }).then( function (ajaxObj) {
        console.log("Ajax obj: ", ajaxObj);
        ppid = ajaxObj.id;
      });
    };
  };
  util.gitlabLogin = function (ev) {
    ev.preventDefault();
    ev.stopPropagation();

    var ppid = '';

    var authObj = window.OAUTH3.core.implicitGrant(
      { authorization_dialog: { url:'https://github.com/login/oauth/authorize' }
      }
    , { redirect_uri: 'https://' + CONFIG.host + '/.well-known/oauth3/callback.html'
      , client_id: 'df4d46a358c1f3519c60'
      }
    );

    var signinW = window.open(
      authObj.url
    , "third-party-provider"
    );

    var callbackName = '--oauth3-callback-' + authObj.state;

    console.log('authobj: ', authObj);
    console.log('callback: ',callbackName);

    window[callbackName] = function (obj) {
      console.log('callback obj: ', obj);
      $.ajax({
        url: "https://api.github.com/user"
      , headers: {authorization: "Bearer " + obj.access_token}
      }).then( function (ajaxObj) {
        console.log("Ajax obj: ", ajaxObj);
        ppid = ajaxObj.id;
      });
    };
  };

  $('body').on('click', '.js-facebook-login', util.facebookLogin);
  //$('body').on('click', '.js-twitter-login', util.twitterLogin);
  $('body').on('click', '.js-google-login', util.googleLogin);
  //$('body').on('click', '.js-github-login', util.githubLogin);
  //$('body').on('click', '.js-gitlab-login', util.gitlabLogin);
  //
  // END 3rd Party Logins
  //
}());
