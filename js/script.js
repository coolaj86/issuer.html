// $('body').on('click', '.js-remember-label', function (ev) {
//   ev.preventDefault();
//   ev.stopPropagation();
//   if ($('.js-remember-label').find('.js-remember-checkbox').prop('checked') === true ) {
//     $('.js-remember-label').find('.js-remember-status').removeClass('fa-check-square-o');
//     $('.js-remember-label').find('.js-remember-status').addClass('fa-square-o dap-remember-margin');
//     $('.js-remember-label').find('.js-remember-checkbox').prop('checked', false);
//     $('.js-remember-btn').removeClass('dap-full-button-purple');
//     $('.js-remember-btn').addClass('dap-full-button-green');
//     $('.js-remember-btn').text('SIGN IN ONCE');
//   } else if ($('.js-remember-label').find('.js-remember-checkbox').prop('checked') === false ) {
//     $('.js-remember-label').find('.js-remember-status').removeClass('fa-square-o dap-remember-margin');
//     $('.js-remember-label').find('.js-remember-status').addClass('fa-check-square-o');
//     $('.js-remember-label').find('.js-remember-checkbox').prop('checked', true);
//     $('.js-remember-btn').removeClass('dap-full-button-green');
//     $('.js-remember-btn').addClass('dap-full-button-purple');
//     $('.js-remember-btn').text('SIGN IN FOREVER');
//   }
// });

// $('body').on('click', '.js-auth-li-enabled', function (ev) {
//   ev.preventDefault();
//   ev.stopPropagation();
//
//   var $checkbox = $(this);
//   if ($checkbox.find('.js-auth-checkbox').prop('checked') === true ) {
//     // $checkbox.removeClass('fa-check-square-o');
//     // $checkbox.addClass('fa-square-o');
//     $checkbox.find('.js-auth-checkbox').prop('checked', false);
//   } else if ($checkbox.find('.js-auth-checkbox').prop('checked') === false ) {
//     // $checkbox.removeClass('fa-square-o');
//     // $checkbox.addClass('fa-check-square-o');
//     $checkbox.find('.js-auth-checkbox').prop('checked', true);
//   }
// });
$('body').on('click', '.js-remember-label', function (ev) {
  'use strict';
  ev.preventDefault();
  ev.stopPropagation();
  var $this = $(this);
  if ($this.find('.js-remember-checkbox').is(':checked') === true) {
    $this.find('.js-remember-checkbox').prop( "checked", false );
  } else {
    $this.find('.js-remember-checkbox').prop( "checked", true );
  }
});

$('body').on('click', '.check', function () {
  'use strict';
});

$('body').on('click', '.js-auth-li-enabled', function (ev) {
  'use strict';
  ev.preventDefault();
  ev.stopPropagation();


  var $this = $(this);
  var $hiddenCheckbox = $this.find('.js-auth-checkbox');
  var $img = $this.find('.check');
  var newStatus = $hiddenCheckbox.prop('checked') ? "unchecked" : "checked";

  if(newStatus === 'checked') {

    $img.attr('src', './img/pressed-check.png');
    $img.addClass("is-checked");
    $hiddenCheckbox.prop( "checked", true );
  } else {
    $img.attr("src", "./img/unpressed-check.png");
    $img.removeClass("is-checked");
    $hiddenCheckbox.prop( "checked", false );
  }
});

$('body').on('keyup keypress', '.js-authn-otp-code', function (e) {
  'use strict';
  // var keyCode = e.keyCode || e.which;
  // var regex = new RegExp('^[0-9 \-]+$');
  // var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
  // var oauthCode = $(this).val().split('-').join('').replace(/\s/g, '');
  //
  // if (!regex.test(key)) {
  //    event.preventDefault();
  //    return false;
  // }
  //
  // if (oauthCode.length > 0) {
  //   oauthCode = oauthCode.match(new RegExp('.{1,4}', 'g')).join("-");
  // }
  //
  // $(this).val(oauthCode);
  //
  // if($(this).val().length === $(this).attr("maxlength")){
  //   $('.submit-btn').prop("disabled", false);
  // }
  if ($(this).val().length === 14) {
    $('.submit-btn').prop('disabled', false);
  } else {
    $('.error-msg').empty();
    $('.submit-btn').prop('disabled', true);
  }
});

$('.js-authn-otp-code').mask('####-####-####');

$('body').on('keyup', '.js-oauth3-email', function () {
  'use strict';
  var emailAddress = $(this).val();
  var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/i);
  var valid = emailRegex.test(emailAddress);
  if (!valid) {
    $('.js-authn-show').prop("disabled", true);
    return false;
  } else {
    $('.js-authn-show').prop("disabled", false);
    return true;
  }
});

$('body').on('blur', '.js-oauth3-email', function () {
  'use strict';
  var emailAddress = $(this).val();
  $('.email-address').text(emailAddress);
});
