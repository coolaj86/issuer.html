issuer.html
===========

| [oauth3.js](https://git.oauth3.org/OAuth3/oauth3.js)
| *issuer.html*
| [issuer.rest.walnut.js](https://git.oauth3.org/OAuth3/issuer.rest.walnut.js)
| [issuer.srv](https://git.oauth3.org/OAuth3/issuer.srv)
| Sponsored by [ppl](https://ppl.family)


This is a browser application which implements the issuer side of the *authorization_dialog* flow for OAuth3.

It may be used client-side only (public key or granted scope syncing will be disabled),
or will the *issuer.rest.walnut.js* APIs on the backend for full functionality.

For use with walnut it must be installed to `/opt/walnut/packages/pages/issuer@oauth3.org`

```bash
git clone git@git.oauth3.org:OAuth3/org.oauth3.git /opt/walnut/packages/pages/issuer@oauth3.org
pushd /opt/walnut/packages/pages/issuer@oauth3.org
bash ./install.sh
popd
```

```bash
echo "issuer@oauth3.org" >> /opt/walnut/var/sites/EXAMPLE.COM
```

This uses the OAuth3 JavaScript SDK `oauth3.js` as a subpackage in
`/opt/walnut/packages/pages/issuer@oauth3.org/assets/oauth3.org`.
